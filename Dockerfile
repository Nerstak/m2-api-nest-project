FROM node:current-alpine3.15
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
RUN npm run build
ADD . /usr/src/app
EXPOSE 3000
CMD ["node", "dist/main"]