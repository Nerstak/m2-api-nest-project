#!/bin/bash

echo "DATABASE_HOST=$DATABASE_HOST" >> .env.test
echo "DATABASE_PORT=$DATABASE_PORT" >> .env.test
echo "DATABASE_USER=$DATABASE_USER" >> .env.test
echo "DATABASE_PASS=$DATABASE_PASS" >> .env.test
echo "DATABASE_NAME=$DATABASE_NAME" >> .env.test
echo "PORT=$PORT" >> .env.test
echo "JWT_KEY=$JWT_KEY" >> .env.test
echo "JWT_EXPIRATION=60m" >> .env.test
echo "DATABASE_SYNC=true" >> .env.test
