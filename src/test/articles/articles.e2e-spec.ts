import * as request from 'supertest';
import { TypeOrmModule } from "@nestjs/typeorm";
import { INestApplication } from "@nestjs/common";
import { User } from "../../users/entities/user.entity";
import { Repository } from "typeorm";
import { UsersService } from "../../users/users.service";
import { Test } from "@nestjs/testing";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { instanceToPlain } from "class-transformer";
import specUtils from "../spec-utils";
import { AuthService } from "../../auth/auth.service";
import { JwtService } from "@nestjs/jwt";
import { PasswordService } from "../../password/password.service";
import { AuthModule } from "../../auth/auth.module";
import { generateCreateUser } from "../fakes/users.fake";
import typeORM from "../../config/typeORM";
import { load } from "../../config/config";
import { ArticlesModule } from "../../articles/articles.module";
import { ArticlesService } from "../../articles/articles.service";
import { Article } from "../../articles/entities/article.entity";
import { generateCreateArticleDTO, generateCreateArticleWithUser } from "../fakes/articles.fake";

describe('ArticlesController (e2e)', () => {
  let authService: AuthService;
  let articlesService: ArticlesService;
  let usersService: UsersService;

  let config: ConfigService;
  let articleRepository: Repository<Article>;
  let userRepository: Repository<User>;

  let app: INestApplication;
  let user: User;
  let wrongUser: User;

  beforeEach(async () => {
    await specUtils.clear();

    user = await usersService.create(generateCreateUser());
    wrongUser = await usersService.create(generateCreateUser());
  });

  afterEach(async () => {
    await specUtils.clear();
  })

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [
        ArticlesModule,
        AuthModule,
        ConfigModule.forRoot(load()),
        TypeOrmModule.forRootAsync(typeORM()),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    articleRepository = moduleFixture.get('ArticleRepository');
    articlesService = new ArticlesService(articleRepository);

    userRepository = moduleFixture.get('UserRepository');
    usersService = new UsersService(userRepository);

    config = new ConfigService();
    authService = new AuthService(
      usersService,
      new JwtService(
        {
          secret: config.get<string>("JWT_KEY"),
          signOptions: {expiresIn: config.get<string>("JWT_EXPIRATION")}}),
      new PasswordService())
  });

  it(`/GET articles`, async () => {
    await articlesService.create(generateCreateArticleWithUser(user));

    await request(app.getHttpServer())
      .get('/articles')
      .expect(200)
      .expect(
        JSON.stringify(instanceToPlain(await articlesService.findAll())),
      );
  });

  it(`/GET articles/:id`, async () => {
    const article = await articlesService.create(generateCreateArticleWithUser(user));

    await request(app.getHttpServer())
      .get('/articles/' + article.id)
      .expect(200)
      .expect(
          JSON.stringify(instanceToPlain({
            id: article.id,
            title: article.title,
            content: article.content,
            author: {
              id: article.author.id,
              username: article.author.username
            }
          })
          )
      );
  });

  describe("/DELETE articles/:id", () => {
    it('Deleting own article', async() => {
      const article = await articlesService.create(generateCreateArticleWithUser(user));

      const jwt = await authService.login(user);

      await request(app.getHttpServer())
        .delete('/articles/' + article.id)
        .set('Authorization', 'Bearer ' + jwt.access_token)
        .expect(200)
        .expect("");
    });

    it('Deleting another user\'s article', async() => {
      const article = await articlesService.create(generateCreateArticleWithUser(wrongUser));

      const jwt = await authService.login(user);

      await request(app.getHttpServer())
        .delete('/articles/' + article.id)
        .set('Authorization', 'Bearer ' + jwt.access_token)
        .expect(401);
    })
  });

  describe("/PATCH articles/:id", () => {
    it('Updating own article', async() => {
      const article = await articlesService.create(generateCreateArticleWithUser(user));

      const jwt = await authService.login(user);

      await request(app.getHttpServer())
        .patch('/articles/' + article.id)
        .set('Authorization', 'Bearer ' + jwt.access_token)
        .send({
          title: "Updated title"
        })
        .expect(200)
        .expect("");
    });

    it('Updating another user\'s article', async() => {
      const article = await articlesService.create(generateCreateArticleWithUser(wrongUser));

      const jwt = await authService.login(user);

      await request(app.getHttpServer())
        .patch('/articles/' + article.id)
        .set('Authorization', 'Bearer ' + jwt.access_token)
        .expect(401);
    })
  });


  it('/POST /articles', async() => {
    const article = generateCreateArticleDTO();

    const jwt = await authService.login(user);

    await request(app.getHttpServer())
      .post('/articles/')
      .set('Authorization', 'Bearer ' + jwt.access_token)
      .send(article)
      .expect(201)
      .expect(
        async () => {
          return JSON.stringify(instanceToPlain(await articlesService.findAll()));
        }
      );
  });


  afterAll(async () => {
    await app.close();
  });
});