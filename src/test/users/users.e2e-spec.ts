import * as request from 'supertest';
import { UsersModule } from "../../users/users.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { INestApplication } from "@nestjs/common";
import { User } from "../../users/entities/user.entity";
import { Repository } from "typeorm";
import { UsersService } from "../../users/users.service";
import { Test } from "@nestjs/testing";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { instanceToPlain } from "class-transformer";
import specUtils from "../spec-utils";
import { AuthService } from "../../auth/auth.service";
import { JwtService } from "@nestjs/jwt";
import { PasswordService } from "../../password/password.service";
import { AuthModule } from "../../auth/auth.module";
import { generateCreateUser } from "../fakes/users.fake";
import typeORM from "../../config/typeORM";
import { load } from "../../config/config";

describe('UsersController (e2e)', () => {
  let authService: AuthService;
  let usersService: UsersService;
  let config: ConfigService;
  let userRepository: Repository<User>;
  let app: INestApplication;

  beforeEach(async () => {
    await specUtils.clear();
  });

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [
        UsersModule,
        AuthModule,
        ConfigModule.forRoot(load()),
        TypeOrmModule.forRootAsync(typeORM()),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    userRepository = moduleFixture.get('UserRepository');
    usersService = new UsersService(userRepository);
    config = new ConfigService();
    authService = new AuthService(
      usersService,
      new JwtService(
        {
          secret: config.get<string>("JWT_KEY"),
          signOptions: {expiresIn: config.get<string>("JWT_EXPIRATION")}}),
      new PasswordService())
  });

  it(`/GET users`, async () => {
    await usersService.create(generateCreateUser());

    const result = request(app.getHttpServer());
    await result
      .get('/users')
      .expect(200)
      .expect(
        JSON.stringify(instanceToPlain(await usersService.findAll())),
      );
  });

  it(`/POST users`, async () => {
    const payload = generateCreateUser();

    await request(app.getHttpServer())
      .post('/users')
      .send(payload)
      .expect(201)
      .expect(
        async () => {
          return JSON.stringify(instanceToPlain(await usersService.findAll()));
          }
        )
  });

  describe('/GET users/me',  () => {
    it(`Getting own user logged in`, async () => {
      const user = await usersService.create(generateCreateUser());

      const jwt = await authService.login(user);

      await request(app.getHttpServer())
        .get('/users/me')
        .set('Authorization', 'Bearer ' + jwt.access_token)
        .expect(200)
        .expect(
          async () => {
            return JSON.stringify(instanceToPlain(await usersService.findOneOrFail(user.id), {groups: ['owner']}));
          }
        );
    });

    it(`Getting own user logged out`, async () => {
      await usersService.create(generateCreateUser());

      await request(app.getHttpServer())
        .delete('/users/me')
        .expect(401);
    });
  });

  it(`/GET users/:id`, async () => {
    const user = await usersService.create(generateCreateUser());

    await request(app.getHttpServer())
      .get('/users/' + user.id)
      .expect(200)
      .expect(
          JSON.stringify(instanceToPlain({
            id: user.id,
            username: user.username
          }))
      );
  });

  it(`/PATCH users/:id`, async () => {
    const user = await usersService.create(generateCreateUser());

    const jwt = await authService.login(user);

    await request(app.getHttpServer())
      .patch('/users/' + user.id)
      .send({
        email: "email@box.com"
      })
      .expect(200)
      .set('Authorization', 'Bearer ' + jwt.access_token)
      .expect("");
  });

  describe('/DELETE users/:id', () => {
    it(`Deleting own user`, async () => {
      const user = await usersService.create(generateCreateUser());

      const jwt = await authService.login(user);

      await request(app.getHttpServer())
        .delete('/users/' + user.id)
        .set('Authorization', 'Bearer ' + jwt.access_token)
        .expect(200)
        .expect("");
    });

    it(`Deleting another user`, async () => {
      const user = await usersService.create(generateCreateUser());
      const toDelete = await usersService.create(generateCreateUser())

      const jwt = await authService.login(user);

      await request(app.getHttpServer())
        .delete('/users/' + toDelete.id)
        .set('Authorization', 'Bearer ' + jwt.access_token)
        .expect(401);
    });
  })

  afterAll(async () => {
    await app.close();
  });
});