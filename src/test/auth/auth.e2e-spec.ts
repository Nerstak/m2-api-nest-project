import * as request from 'supertest';
import { UsersModule } from "../../users/users.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { INestApplication } from "@nestjs/common";
import { User } from "../../users/entities/user.entity";
import { Repository } from "typeorm";
import { UsersService } from "../../users/users.service";
import { Test } from "@nestjs/testing";
import { ConfigModule } from "@nestjs/config";
import specUtils from "../spec-utils";
import { PasswordService } from "../../password/password.service";
import { AuthModule } from "../../auth/auth.module";
import typeORM  from "../../config/typeORM";
import { load } from "../../config/config";

describe('AuthController (e2e)', () => {
  let userRepository: Repository<User>;
  let app: INestApplication;

  beforeEach(async () => {
    await specUtils.clear();
  });

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [
        UsersModule,
        PasswordService,
        AuthModule,
        ConfigModule.forRoot(load()),
        TypeOrmModule.forRootAsync(typeORM()),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    userRepository = moduleFixture.get('UserRepository');
    new UsersService(userRepository);
  });

  it(`/POST auth/login`, async () => {
    const payload = {
      email: "mail@mail.com",
      password: "ImHash",
      username: "username"
    }
    // We make a request here, to be sure that the password is processed within controller
    await request(app.getHttpServer()).post('/users').send(payload).expect(201)

    await request(app.getHttpServer())
      .post('/auth/login')
      .send(payload)
      .expect(201);
  });

  afterAll(async () => {
    await app.close();
  });
});