import * as faker from "faker";
import { User } from "../../users/entities/user.entity";
import { CreateArticleDto, CreateArticleWithUser } from "../../articles/dto/create-article.dto";

export function generateCreateArticleWithUser(user?: User): CreateArticleWithUser {
  return {
    title: faker.lorem.sentence(),
    content: faker.lorem.sentence(),
    author: user
  }
}

export function generateCreateArticleDTO(): CreateArticleDto {
  return {
    title: faker.lorem.sentence(),
    content: faker.lorem.sentence()
  }
}