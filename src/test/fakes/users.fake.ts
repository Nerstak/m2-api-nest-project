import * as faker from "faker";

export function generateCreateUser() {
  return {
    email: faker.internet.email(),
    password: faker.internet.password(),
    username: faker.internet.userName()
  }
}