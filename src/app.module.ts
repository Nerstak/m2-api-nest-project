import { Module } from '@nestjs/common';
import { ConfigModule } from "@nestjs/config";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { PasswordModule } from './password/password.module';
import typeORM from "./config/typeORM";
import { load } from "./config/config";
import { ArticlesModule } from './articles/articles.module';
import { CaslModule } from './casl/casl.module';

@Module({
  imports: [
    ConfigModule.forRoot(load()),
    TypeOrmModule.forRootAsync(typeORM()),
    UsersModule,
    AuthModule,
    PasswordModule,
    ArticlesModule,
    CaslModule
  ]
})
export class AppModule {}
