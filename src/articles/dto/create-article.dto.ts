import { OmitType } from "@nestjs/swagger";
import { Article } from "../entities/article.entity";

export class CreateArticleWithUser extends OmitType(Article, ["id", "authorId"]) {}
export class CreateArticleDto extends OmitType(CreateArticleWithUser, ["author"]) {}