import {
  Body,
  ClassSerializerInterceptor,
  Controller, Delete,
  Get, InternalServerErrorException, NotFoundException, Param, Patch,
  Post,
  Req, UnauthorizedException, UnprocessableEntityException,
  UseInterceptors
} from "@nestjs/common";
import {
  ApiCreatedResponse, ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnprocessableEntityResponse
} from "@nestjs/swagger";
import { CreateArticleDto, CreateArticleWithUser } from "./dto/create-article.dto";
import { RequestWithUserDto } from "../auth/dto/RequestWithUser.dto";
import { ArticlesService } from "./articles.service";
import { CaslAbilityFactory } from "../casl/casl-ability.factory";
import { Action } from "../casl/action.enum";
import { UpdateArticleDto } from "./dto/update-article.dto";
import { ExceptionDto } from "../exceptions/exception.dto";
import { Article } from "./entities/article.entity";
import { Auth } from "../auth/auth.decorator";

@Controller('articles')
@ApiTags('Articles')
@UseInterceptors(ClassSerializerInterceptor)
export class ArticlesController {
  constructor(
    private articlesService: ArticlesService,
    private caslAbilityFactory: CaslAbilityFactory
  ) {}

  @Post()
  @Auth()
  @ApiOperation({summary: 'Create an article'})
  @ApiCreatedResponse({type: Article})
  @ApiUnprocessableEntityResponse({
    description: MessagesConstant.UNPROCESSABLE_EXCEPTION,
    type: ExceptionDto
  })
  create(@Req() req: RequestWithUserDto, @Body() article: CreateArticleDto) {
    const createArticleWithUser: CreateArticleWithUser = {
      content: article.content,
      title: article.title,
      author: req.user
    }

    try {
      return this.articlesService.create(createArticleWithUser);
    } catch (e) {
      throw new UnprocessableEntityException();
    }
  }

  @Get()
  @ApiOperation({ summary: 'List all articles'})
  @ApiOkResponse({type: Article})
  async findAll() {
    return this.articlesService.findAll();
  }

  @Get(':id')
  @ApiOperation({ summary: 'Find an article' })
  @ApiOkResponse({type: Article})
  @ApiNotFoundResponse({
    description: MessagesConstant.NOTFOUND_EXCEPTION,
    type: ExceptionDto
  })
  async findOne(@Param('id') id: string) {
    try {
      return await this.articlesService.findOneOrFail(id);
    } catch (e) {
      throw new NotFoundException();
    }
  }

  @Patch(':id')
  @Auth()
  @ApiOperation({ summary: 'Update an article'})
  @ApiOkResponse()
  @ApiNotFoundResponse({
    description: MessagesConstant.NOTFOUND_EXCEPTION,
    type: ExceptionDto
  })
  async update(@Req() req: RequestWithUserDto, @Param('id') id: string, @Body() updateArticleDto: UpdateArticleDto) {
    if(!this.caslAbilityFactory.createForUser(req.user).can(Action.Update, await this.findOne(id))) {
      throw new UnauthorizedException('Cannot update article from other users');
    }
    if((await this.articlesService.update(id, updateArticleDto)).affected !== 1) {
      throw new InternalServerErrorException();
    }
  }

  @Delete(':id')
  @Auth()
  @ApiOperation({ summary: 'Delete an article'})
  @ApiNotFoundResponse({
    description: MessagesConstant.NOTFOUND_EXCEPTION,
    type: ExceptionDto
  })
  async delete(@Req() req: RequestWithUserDto, @Param('id') id: string) {
    if(!this.caslAbilityFactory.createForUser(req.user).can(Action.Delete, await this.findOne(id))) {
      throw new UnauthorizedException('Cannot delete article from other users');

    }
    const articleRemoved = await this.articlesService.remove(id);
    if(!articleRemoved) {
      throw new Error('No article removed');
    }
  }
}
