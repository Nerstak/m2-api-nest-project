import { Injectable } from '@nestjs/common';
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Article } from "./entities/article.entity";
import { CreateArticleWithUser } from "./dto/create-article.dto";
import { UpdateArticleDto } from "./dto/update-article.dto";

@Injectable()
export class ArticlesService {
  constructor(@InjectRepository(Article) private articleRepository: Repository<Article>) {}

  create(createArticleWithUser: CreateArticleWithUser) {
    return this.articleRepository.save(createArticleWithUser);
  }

  findAll() {
    return this.articleRepository.find();
  }

  findOne(id: string) {
    return this.articleRepository.findOne(id);
  }

  findOneOrFail(id: string) {
    return this.articleRepository.findOneOrFail(id);
  }

  update(id: string, updateArticleDto: UpdateArticleDto) {
    return this.articleRepository.update(id, updateArticleDto);
  }

  async remove(id: string) {
    const article = await this.articleRepository.findOneOrFail(id);
    return this.articleRepository.remove(article);
  }
}
