import { Column, Entity, ManyToOne, PrimaryGeneratedColumn, RelationId } from "typeorm";
import { User } from "../../users/entities/user.entity";
import { Exclude } from "class-transformer";
import { IsNotEmpty, IsString, IsUUID, MaxLength } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

@Entity()
export class Article {
  @PrimaryGeneratedColumn("uuid")
  @IsUUID()
  @ApiProperty({description: "Article's id"})
  id: string;

  @Column({length: "150"})
  @IsString()
  @MaxLength(150)
  @ApiProperty({
    description: "Article's title"
  })
  title: string;

  @Column()
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: "Article's content"
  })
  content: string;

  @ManyToOne(() => User,{ onDelete: 'CASCADE' , eager: true})
  author: User

  @Exclude()
  @RelationId((article: Article) => article.author)
  authorId: string
}