import { ConfigModuleOptions } from "@nestjs/config";

export function load(): ConfigModuleOptions {
  const NODE_ENV = (process.env.NODE_ENV || 'development') as 'test' | 'development' | 'production'

  return {
    isGlobal: true,
    envFilePath: NODE_ENV === 'test' ? '.env.test' : '.env'
  }
}