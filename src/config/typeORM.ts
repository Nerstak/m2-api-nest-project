import { User } from "../users/entities/user.entity";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { Article } from "../articles/entities/article.entity";

function typeORMOptions(config: ConfigService): TypeOrmModuleOptions {
  return {
    type: "postgres",
    host: getOrThrow(config, "DATABASE_HOST"),
    port: parseInt(getOrThrow(config,"DATABASE_PORT") || '5432',10),
    database: getOrThrow(config, "DATABASE_NAME"),
    username: getOrThrow(config, "DATABASE_USER"),
    password: getOrThrow(config, "DATABASE_PASS"),
    logging: getOrThrow(config,"DATABASE_LOGGING", "") === "true",
    entities: [User, Article],
    migrationsRun: getOrThrow(config, "DATABASE_MIGRATION", "") === "true",
    synchronize: getOrThrow(config, "DATABASE_SYNC", "") === "true"
  }
}

/**
 * Get an environment variable, or throw
 * @param config Config Service
 * @param name Name of variable
 * @param opt Value to return if variable is not found (instead of throwing)
 */
function getOrThrow(config: ConfigService, name: string, opt?: string) {
  const val = config.get<string>(name);
  if (typeof val === 'undefined') {
    if(opt === undefined) {
      throw new Error(`Missing mandatory environment variable ${name}`);
    } else {
      return opt;
    }
  }
  return val;
}

export default () => (
  {
    imports: [ConfigModule],
    useFactory: (config: ConfigService) => typeORMOptions(config),
    inject: [ConfigService]
  }
)