import { Injectable } from '@nestjs/common';
import * as argon2 from "argon2";

/**
 * Service for password checking
 * Note: this current version use Argon2i
 */
@Injectable()
export class PasswordService {
  verify(hash: string, plain: string) {
    return argon2.verify(hash, plain);
  }

  hash(plain: string) {
    return argon2.hash(plain);
  }
}
