export enum Action {
  Read = 'read',
  Update = 'update',
  Delete = 'delete',
}
