// eslint-disable-next-line @typescript-eslint/no-unused-vars
const enum MessagesConstant {
  UNAUTHORIZED_EXCEPTION = "Unauthorized operation",
  NOTFOUND_EXCEPTION = "Entity not found",
  UNPROCESSABLE_EXCEPTION = "Entity cannot be saved"
}