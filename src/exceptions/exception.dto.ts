import { ApiProperty } from "@nestjs/swagger";

export class ExceptionDto {
  @ApiProperty({description: "Http Status Code"})
  statusCode: number;

  @ApiProperty({description: "Error description"})
  error?: string;

  @ApiProperty({description: "Additional message"})
  message?: string[] | string
}