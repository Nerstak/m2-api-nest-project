import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get, InternalServerErrorException,
  NotFoundException,
  Param,
  Patch,
  Post,
  Req, SerializeOptions,
  UnauthorizedException,
  UnprocessableEntityException,
  UseInterceptors
} from "@nestjs/common";
import { UsersService } from "./users.service";
import { CreateUserDto } from "./dto/create-user.dto";
import { UpdateUserDto } from "./dto/update-user.dto";
import {
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  ApiUnprocessableEntityResponse
} from "@nestjs/swagger";
import { User } from "./entities/user.entity";
import { RequestWithUserDto } from "../auth/dto/RequestWithUser.dto";
import { PasswordService } from "../password/password.service";
import { CaslAbilityFactory } from "../casl/casl-ability.factory";
import { Action } from "../casl/action.enum";
import { ExceptionDto } from "../exceptions/exception.dto";
import { Auth } from "../auth/auth.decorator";


// Note: rather than having two routes for user fetching, I would've liked to have the findOne route to return email
// field IF the current user is the owner
// Like this: https://stackoverflow.com/questions/52850574/how-to-expose-field-for-the-current-user-only#comment117982548_52873354
// I was not able to implement this reflection, thus the two routes (findOne, findCurrent)
// An in-depth analysis of how NestJS applies SerializeOptions with Reflection would open the door to 'hijack' their system, and
// have Serialization options defined at runtime

@Controller('users')
@ApiTags('Users')
@UseInterceptors(ClassSerializerInterceptor)
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly passwordService: PasswordService,
    private caslAbilityFactory: CaslAbilityFactory
    ) {}

  @Post()
  @ApiOperation({summary: "Create an user"})
  @ApiCreatedResponse({
    type: User
  })
  @ApiUnprocessableEntityResponse({
    description: MessagesConstant.UNPROCESSABLE_EXCEPTION,
    type: ExceptionDto
  })
  async create(@Body() createUserDto: CreateUserDto) {
    createUserDto.password = await this.passwordService.hash(createUserDto.password);

    // Because the create method returns a union btw the Entity and the Dto, the
    // ClassSerializerInterceptor is not able to process it
    try {
      const user = await this.usersService.create(createUserDto)
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...result } = user;
      return result;
    } catch (e) {
      throw new UnprocessableEntityException();
    }
  }

  @Get()
  @ApiOperation({summary: 'Find all users'})
  @ApiOkResponse({type: User})
  async findAll() {
    return this.usersService.findAll();
  }

  @Get('me')
  @Auth()
  @SerializeOptions({groups: ['owner']})
  @ApiOperation({summary: 'Find current logged in user'})
  @ApiOkResponse({type: User})
  @ApiNotFoundResponse({
    description: MessagesConstant.NOTFOUND_EXCEPTION,
    type: ExceptionDto
  })
  async findCurrent(@Req() req: RequestWithUserDto) {
    try {
      return await this.usersService.findOneOrFail(req.user.id);
    } catch (e) {
      throw new NotFoundException();
    }
  }

  @Get(':id')
  @ApiOperation({summary: 'Find an user'})
  @ApiOkResponse({type: User})
  @ApiNotFoundResponse({
    description: MessagesConstant.NOTFOUND_EXCEPTION,
    type: ExceptionDto
  })
  async findOne(@Param('id') id: string) {
    try {
      return await this.usersService.findOneOrFail(id);
    } catch (e) {
      throw new NotFoundException();
    }
  }

  @Patch(':id')
  @Auth()
  @ApiOperation({summary: 'Update an user'})
  @ApiOkResponse()
  @ApiNotFoundResponse({
    description: MessagesConstant.NOTFOUND_EXCEPTION,
    type: ExceptionDto
  })
  async update(@Req() req: RequestWithUserDto, @Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    if(!this.caslAbilityFactory.createForUser(req.user).can(Action.Update, await this.findOne(id))) {
      throw new UnauthorizedException('Cannot update other users');
    }
    if(updateUserDto.password) {
      updateUserDto.password = await this.passwordService.hash(updateUserDto.password);
    }

    if((await this.usersService.update(id, updateUserDto)).affected !== 1) {
      throw new InternalServerErrorException();
    }
  }

  @Delete(':id')
  @Auth()
  @ApiOperation({summary: 'Delete an user'})
  @ApiOkResponse()
  @ApiNotFoundResponse({
    description: MessagesConstant.NOTFOUND_EXCEPTION,
    type: ExceptionDto
  })
  async remove(@Req() req: RequestWithUserDto, @Param('id') id: string) {
    if(!this.caslAbilityFactory.createForUser(req.user).can(Action.Delete, await this.findOne(id))) {
      throw new UnauthorizedException('Cannot delete other users');
    }

    const userRemoved = await this.usersService.remove(id);
    if(!userRemoved) {
      throw new Error('No user removed');
    }
  }
}
