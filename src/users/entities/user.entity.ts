import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { Exclude, Expose } from "class-transformer";
import { IsEmail, IsNotEmpty, IsString, IsUUID, MaxLength } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

@Entity()
export class User {
  @PrimaryGeneratedColumn("uuid")
  @IsUUID()
  @ApiProperty({description: "User id"})
  id: string;

  @Column({length: "50"})
  @IsString()
  @IsNotEmpty()
  @MaxLength(50)
  @ApiProperty({
    description: "Username"
  })
  username: string;

  @Column({length: "50", unique: true})
  @Exclude({ toPlainOnly: true })
  @IsEmail()
  @MaxLength(50)
  @ApiProperty({
    description: "User email, unique"
  })
  email: string;

  @Expose({ groups: ['owner']})
  @ApiProperty({
    description: "User email, only available on /users/me",
    type: "string"
  })
  get userEmail() {
    return this.email;
  }

  @Column()
  @Exclude({ toPlainOnly: true })
  @IsNotEmpty()
  @ApiProperty({description: "User password"})
  password: string;
}
