import { ApiProperty } from "@nestjs/swagger";

export class JwtTokenDto {
  @ApiProperty({description: "JWT Token"})
  access_token: string;
}