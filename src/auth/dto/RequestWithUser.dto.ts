import { User } from "../../users/entities/user.entity";

export type RequestWithUserDto = Request & {user: User}