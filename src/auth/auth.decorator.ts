import { applyDecorators, UseGuards } from "@nestjs/common";
import { ApiBearerAuth, ApiUnauthorizedResponse } from "@nestjs/swagger";
import { JwtAuthGuard } from "./jwt-auth.guard";
import { ExceptionDto } from "../exceptions/exception.dto";

export function Auth() {
  return applyDecorators(
    ApiBearerAuth(),
    UseGuards(JwtAuthGuard),
    ApiUnauthorizedResponse({
      description: MessagesConstant.UNAUTHORIZED_EXCEPTION,
      type: ExceptionDto
    })
  );
}