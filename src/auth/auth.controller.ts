import {
  Controller,
  Post,
  UseGuards,
  Body,
  UseInterceptors,
  ClassSerializerInterceptor
} from "@nestjs/common";
import { LocalAuthGuard } from "./local-auth.guard";
import { AuthService } from "./auth.service";
import { LoginDto } from "./dto/login.dto";
import { ApiCreatedResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse } from "@nestjs/swagger";
import { JwtTokenDto } from "./dto/jwtTokenDto";
import { ExceptionDto } from "../exceptions/exception.dto";

@Controller()
@UseInterceptors(ClassSerializerInterceptor)
@ApiTags('Authentication')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('auth/login')
  @UseGuards(LocalAuthGuard)
  @ApiOperation({summary: 'Log in as an user'})
  @ApiCreatedResponse({type: JwtTokenDto})
  @ApiUnauthorizedResponse({
    description: MessagesConstant.UNAUTHORIZED_EXCEPTION,
    type: ExceptionDto
  })
  async login(@Body() login: LoginDto) {
    return await this.authService.login(login);
  }
}
