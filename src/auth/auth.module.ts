import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersModule } from "../users/users.module";
import { PassportModule } from "@nestjs/passport";
import { LocalStrategy } from "./local.strategy";
import { AuthController } from './auth.controller';
import { JwtModule } from "@nestjs/jwt";
import { ConfigService } from "@nestjs/config";
import { JwtStrategy } from "./jwt.strategy";
import { PasswordModule } from "../password/password.module";

@Module({
  providers: [AuthService, LocalStrategy, JwtStrategy],
  imports: [
    UsersModule,
    PasswordModule,
    PassportModule,
    JwtModule.registerAsync({
        useFactory: (config: ConfigService) => {
          return {
            secret: config.get("JWT_KEY"),
            signOptions: {
              expiresIn: config.get("JWT_EXPIRATION")
            }
          };
        },
        inject: [ConfigService]
    })
  ],
  controllers: [AuthController]
})
export class AuthModule {}
