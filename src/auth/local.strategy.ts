import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { AuthService } from "./auth.service";
import { Injectable, UnauthorizedException } from "@nestjs/common";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({usernameField: "email"});
  }

  /**
   * Validate an user
   * @param email Email
   * @param password Password
   * @return User if found & validated
   */
  async validate(email: string, password: string) {
    const user = await this.authService.validateUser(email, password);
    if(!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}