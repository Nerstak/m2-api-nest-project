import { Injectable } from "@nestjs/common";
import { UsersService } from "../users/users.service";
import { JwtService } from "@nestjs/jwt";
import { JwtTokenDto } from "./dto/jwtTokenDto";
import { LoginDto } from "./dto/login.dto";
import { PasswordService } from "../password/password.service";
import { User } from "../users/entities/user.entity";

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private passwordService: PasswordService
  ) {}

  /**
   * Validate a user
   * @param email Email
   * @param pass Password
   * @return User
   */
  async validateUser(email: string, pass: string): Promise<User> {
    const user = await this.usersService.findOneEmail(email);
    if (user && await this.passwordService.verify(user.password, pass)) {
      return user;
    }
    return null;
  }

  /**
   * Generate a JWT Token for login
   * @param login LoginDTO (email + password)
   * @return JWT Token
   */
  async login(login: LoginDto): Promise<JwtTokenDto> {
    const user = await this.usersService.findOneEmail(login.email);
    const payload = {
      email: user.email,
      sub: user.id
    }

    return {
      access_token: this.jwtService.sign(payload)
    };
  }
}
