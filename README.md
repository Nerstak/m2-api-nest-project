# M2 Api Nest Project: Blogify

## Table of contents

- [Information](#Information)

- [Features](#Features)

- [Installation](#Installation)

- [Tests](#Tests)

## Information

### Description

Building a small blog API. The project was mainly focused on using the best practices with NestJS.
See the API description [here](https://gitlab.com/Nerstak/m2-api-nest-project/-/blob/main/swagger-spec.json).

Checkout the pipeline [here](https://gitlab.com/Nerstak/m2-api-nest-project/-/blob/main/.gitlab-ci.yml).
For checking the latest `lint` and `test` jobs, see [here](https://gitlab.com/Nerstak/m2-api-nest-project/-/jobs/) and search for the most recent ones.

You can see how I solved the requirements in the [report](https://gitlab.com/Nerstak/m2-api-nest-project/-/blob/main/REPORT.md).

### Technologies used

Build with:

- NodeJS & NestJS

- TypeScript

Database:

- PostgresSQL

Integration:

- Gitlab CI/CD

- Docker

## Features

The application developed is a rather simple project, as it is an API to publish, edit and delete blog posts, using their own user accounts.

A non identified user can:

- Create an account

- List the accounts (and see a specific one)

- List the articles (and see a specific one)

- Log in into an existing account

An identified user can:

- Update / Delete its account

- See its own account private information

- Publish / Update / Delete its articles

## Installation

Check out first the [Configuration](#Configuration) section first, then the installation process for your needs.

### Configuration

Running the application with `NODE_ENV=test` will load the `.env.test` file. Otherwise, the `.env` file is loaded.
See the [`.env.example`](https://gitlab.com/Nerstak/m2-api-nest-project/-/blob/main/.env.example) for an example.
The following variables in the `.env` file will be taken used for the configuration of the application.

| Name               | Is Mandatory | Accepted values                                         | Notes                                                                                                    |
|--------------------|--------------|---------------------------------------------------------|----------------------------------------------------------------------------------------------------------|
| PORT               | Yes          | Positive number                                         | The port given must be free for the NestJS application                                                   |
| JWT_KEY            | Yes          | String                                                  | JWT Secret Key                                                                                           |
| JWT_EXPIRATION     | Yes          | String, using [ms format](https://github.com/vercel/ms) | Time of expiration after token was issued                                                                |
| DATABASE_HOST      | Yes          | IP, domain name                                         | Host of database                                                                                         |
| DATABASE_PORT      | Yes          | Positive number                                         | Port of database                                                                                         |
| DATABASE_NAME      | Yes          | String                                                  | Name of database                                                                                         |
| DATABASE_USER      | Yes          | String                                                  | Name of user                                                                                             |
| DATABASE_PASS      | Yes          | String                                                  | Password of user                                                                                         |
| DATABASE_LOGGING   | No           | `true`, or any other value for false                    | Active logging database                                                                                  |
| DATABASE_MIGRATION | No           | `true`, or any other value for false                    | Indicates if migrations should be auto run on every application launch                                   |
| DATABASE_SYNC      | No           | `true`, or any other value for false                    | Indicates if database schema should be auto created on every application launch. Dont' use in production |

### Development

For development purpose:

- Install a PostgreSQL database. For a docker hosted one, you may use the following command: `docker run --name pg-docker -e POSTGRES_PASSWORD=postgres_pwd -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data postgres`

- Clone the repository

- Run `npm install` inside the cloned repository

- Set up a `.env` file, and eventually a `.env.test` for tests (see [`.env.example`](https://gitlab.com/Nerstak/m2-api-nest-project/-/blob/main/.env.example) for help)

- Run `npm run build`

- Run `npm run start`

### Production

#### Dockerfile

A Dockerfile is provided, and only creates an image for the application. You still have to set up a PostgresSQL database.

- Install a PostgreSQL database (see [Development](#Development) section for more details).

- Clone the repository

- Set up a `.env` file  (see [`.env.example`](https://gitlab.com/Nerstak/m2-api-nest-project/-/blob/main/.env.example) for help). **Don't use *localhost* or *127.0.0.1* inside the environment file**

- Run `docker build -t blogify .`

- Run `docker run --env-file .\.env -p 3000:3000 -d blogify`

## Tests

This project only contains integration tests. Because there is little business logic, this is not big problem. It requires a live database, and configuration is loaded from the `.test.env` file.

- Run `npm run build`

- Run `npm run test:e2e`